/*
* INSERTS TABLA HORARI
*/





/*
* INSERTS TABLA PELICULA
*/
INSERT INTO PELICULA (titol, sinopsi, duracio, director, anyEstrena) VALUES ('Star Wars: Episode IV - A New Hope', "Luke Skywalker joins forces with a Jedi Knight, a cocky pilot, a Wookiee and two droids to save the galaxy from the Empire's world-destroying battle station, while also attempting to rescue Princess Leia from the evil Darth Vader", 125, 'George Lucas', 1977);
INSERT INTO PELICULA (titol, sinopsi, duracio, director, anyEstrena) VALUES ('The Predator', "When a young boy accidentally triggers the universe's most lethal hunters' return to Earth, only a ragtag crew of ex-soldiers and a disgruntled scientist can prevent the end of the human race.", 107, 'Shane Black', 2018);

/*
* INSERTS TABLA SALA
*/

INSERT INTO SALA (numSala, totalButacas) VALUES (1, 120);

/*
* INSERTS TABLA SESSION
*/

INSERT INTO SESSIO (idSessio, sessioEspecial, diaSessio, horaSessio, titolPeli, salaSessio) VALUES (1, 0, '2018-12-11', '12:04:00', 'The Predator', 1);


/*
* INSERTS TABLA USUARIO
*/

INSERT INTO USUARI (nomUsuari, cognomUsuari, telefon, email) VALUES ('Victor', 'Vera', 123456789, 'a15vicvercan@iam.cat');

/*
* INSERTS TABLA ENTRADA
*/

INSERT INTO ENTRADA (idEntrada, preu, numButaca, idSessio, email, numSala) VALUES (1, 6, 23, 1, 'a15vicvercan@iam.cat', 1);