USE a15vicvercan_cine;
DROP TABLE IF EXISTS HORARI;
CREATE TABLE HORARI (
	dia date,
    hora time,
    PRIMARY KEY (dia, hora)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
DROP TABLE IF EXISTS PELICULA;
CREATE TABLE PELICULA (
	titol varchar(100),
    sinopsi varchar(500),
    duracio int,
    director varchar(100),
    anyEstrena int,
    cartell varchar(200),
    PRIMARY KEY(titol)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
DROP TABLE IF EXISTS SALA;
CREATE TABLE SALA (
	numSala int,
    totalButacas int,
    PRIMARY KEY (numSala)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
DROP TABLE IF EXISTS SESSIO;
CREATE TABLE SESSIO (
	idSessio int AUTO_INCREMENT,
    sessioEspecial boolean,
    sessioVip boolean,
    diaSessio date,
    horaSessio time,
    titolPeli varchar(100),
    salaSessio int,
    PRIMARY KEY (idSessio),
    FOREIGN KEY (diaSessio, horaSessio) REFERENCES HORARI(dia, hora),
    FOREIGN KEY (titolPeli) REFERENCES PELICULA(titol),
    FOREIGN KEY (salaSessio) REFERENCES SALA(numSala)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
DROP TABLE IF EXISTS USUARI;
CREATE TABLE USUARI (
	nomUsuari varchar(100),
    cognomUsuari varchar(100),
    telefon int(9),
    email varchar(100),
	PRIMARY KEY (email)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
DROP TABLE IF EXISTS ENTRADA;
CREATE TABLE ENTRADA (
	idEntrada int AUTO_INCREMENT,
    preu int,
    numButaca varchar(10),
    idSessio int,
    email varchar(100),
    numSala int, 
	PRIMARY KEY (idEntrada),
    FOREIGN KEY (idSessio) REFERENCES SESSIO(idSessio),
    FOREIGN KEY (email) REFERENCES USUARI(email),
    FOREIGN KEY (numSala) REFERENCES SALA(numSala)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
