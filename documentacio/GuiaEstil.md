#GUIA D'ESTIL
##I AM CINEMA

####1. Logotip
Els logotips proporcionats pertanyen al centre educatiu Ausiàs March [iam.cat](https://iam.cat) i al propi cinema. [Descarrega els logotips](https://drive.google.com/drive/folders/1OvyldzUYf7tV-8iEB_SAu1NaNkyg-M2F?usp=sharing)

####2. Capçalera
A les capçaleres el logotip de l'IAM és l'anomenat
> IAM_CAT_logos_purple_rgb_1.png **amb 100pixels d'amplada.**

A continuació inserir la imatge amb el nom del cinema
> titolCinema.png **amb 180pixels d'amplada.**

El fons de la capçalera és **#63639c** i aquesta ocupa tota l'amplada de la pantalla amb un padding de **20px**.
Qualsevol butó a la capçalera serà de color **XXXX**.

####3. Peu
El peu de pàgina ha de ser amb el color **#63639c**  de fons, amb un padding de **20px**.
En aquest ha d'apareixer centrat i de color blanc el següent text:
> © 2018, CineDAW. Tots els drets reservats.
Tots els drets reservats a Adrián Mellado, Víctor Vera, Cristian Ayala.
Web produïda i dissenyada per alumnes de l'institut IAM.

####4. Tipografia
La font que utilitzem a la web del cinema és la Font Roboto. [Ves-hi](https://fonts.google.com/specimen/Roboto)
Aquesta compta tant amb un font-style com font-weight molt ampli.

####5. Colors
Els dos colors principals son el **#63639c** i **#FF9B21**.
El color del fons és **#F7F7F7** i el del contingut **#FFFFFF**.
El del header i footer és el **#63639c**

#####5.1 Codis del lila
Utilitzem els següents colors per enfosquir i aclarir el lila corporatiu. **#282840 #51517F #9292E5 #A2A2FF**.
Per als textos, icones i botons utilitzem **#333333**.

####6. Retícules
El header i footer van de banda a banda de la pantalla. Amb un padding de **20px**.
El contingut en canvi, l'amplada és del **90%** i te el padding del **2%**. Aquest té una hombra als laterals.

####7. Botons
Els botons tenen color de fons blanc i text en **#FF9B21**.
Els que estan al contingut i són per anar a la pàgina anterior estàn a la esquerra, per avançar a la dreta. Generalment es troben sota el contingut.

####8. Textos i títols
El color que s’utilitza per als textos és el **#333333** i per als títols grans el **#282828**.
L'h1, h2, h3, h4 són small-caps, és a dir, mostrar les minúscules com a majúscules però reduides.

El tamany per a:
> h1 -> 55px.
h2 -> 45px.
h3 -> 35px.
h4 -> 25px.

####10. Imatges
#####10.1 Cartells
Les imatges dels cartells a la pàgina de la cartellera fan **300px**. Tenen un radi de EsqSup i DretaInf **10px** i EsqInf i DretaSup **40px**.
El cartell dins de la informació específica d'aquest fa **250px** d'amplada i el separa de la sinopsis i altres informacions de la pel·lícula a un **5%** de l'esquerra.
Les imatges dels cartells també són al carrusel amb el mateix radi a la bora.

#####10.2 Butaques
Les butaques i la pantalla es poden [descarregar aquí](https://drive.google.com/drive/folders/1KoM0mw36eOF5fe79qal3Ux-j9QOLYYdt?usp=sharing). Si es selecciona una butaca cal cambiar el color a verd. Les imatges ja tenen la mida necessària.

####11. Taules
Les taules agafen per defecte l'estil predefinit per Skeleton (*framework* de CSS lleuger). A més, cal que estiguin centrades.

####12. Calendaris
El calendari no deixa de ser una taula i per tant, també li dona estil Skeleton. Cal afegir el color **#FF9B21** al dia actual i baixar el to a les cel·les de dies anteriors a l'actual.
S'han d'afegir dos botons als costats per tal de anar un mes endevant o enrere.

####13.Gràfics
A la part *Admin* es mostra un gràfic que es genera gràcies a [Charts](https://developers.google.com/chart/). Mostrarem les entrades venudes i les lliures.
El gràfic ha de ser en 3D, amb llegenda i a la gràfica es mostrin percentatges.

####14. Formularis
Les taules agafen per defecte l'estil predefinit per Skeleton (*framework* de CSS lleuger). A més, cal que estiguin centrats. I al formulari que demana el email cal avisar amb **#F78181** si aquest té alguna errada.