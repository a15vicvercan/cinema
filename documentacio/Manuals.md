#Manual d'ús

Només obrir la nostra aplicació, a l'índex hi trobem:
    - Un carrusel amb 10 imatges.
    - Un calendari.
    - Tres botons al header (cartellera, les meves entrades i administració).

Per poder comprar les entrades, podem anar per data de la sessió (fent clic al dia del calendari que volem anar), fent clic a la imatge de la pel·lícula que volem veure del carrusel, o fent clic al botó cartellera.

Si seleccionem la data del calendari, enviaria a l'usuari a una pàgina on demana el mail on validaria que no tens entrades futures, en cas que l'usuari tingui entrades per a sessions futures, s'obrirà una pàgina mostrant les entrades, en cas contrari mostrarà la pel·lícula que es projectarà aquell dia i la informació, i si fem clic al botó "Següent" ens mostraria el pati de butaques de la pel·lícula.

Si seleccionen una pel·lícula del carrusel o de la cartellera, primer ens mostrarà la informació de la pel·lícula i sota una taula amb totes les sessions futures de la pel·lícula (data i hora). Per seleccionar una sessió es faria clic a la data i enviaria a l'usuari a la pàgina on demana el mail i fa la validació, si no te entrades futures ens mostraria el pati de butaques de la pel·lícula.

Al pati de butaques es poden seleccionar les butaques que no estan ocupades, amb un màxim de 10. Al costat va apareixent les butaques seleccionades i el preu total. Quan ja hem seleccionat les butaques fem clic a Següent.

Per finalitzar, apareixerà un formulari per introduir nom, cognom i telèfon. Aquest formulari valida que el telèfon sigui vàlid. Si tot ha anat bé, apareixerà una pàgina amb un missatge indicant que s'han comprat les entrades, en cas contrari apareixerà un Error.

Des de la pàgina índex també es poden comprovar si tens entrades per a una sessió futura. Per comprovar-ho, fem clic al botó del header "Les teves entrades", i introduir el mail. Si té entrades apareixeran en pantalla, en cas contrari, sortirà un error indicant que l'usuari no existeix o que no té entrades.

A la part d'administració, per poder accedir, hem d'anar a la pàgina índex i fer clic al botó "Administració". Apareixerà un formulari de login del apache on s'ha d'introduir l'usuari (admin) i contrasenya (ausias). Si és correcte, apareixerà una pàgina amb un calendari per seleccionar la sessió que volem veure les entrades venudes (normals, VIPs), el pati de butaques i la recaptació per tipus de butaques i total.


# Manual D'instal·lació

Per poder desplegar la nostra aplicació, primer hem d'agafar la URL del nostre repositori per poder clonar-lo al labs.
A continuació accedim a través de SSH al nostre labs, ens posicionem al directori on volem que hi estigui la nostra aplicació i executem la següent comanda: git clone (URL del nostre repositori git).

Per poder accedir a la part d'administrador de la nostra aplicació utilitzem el login d'apache, per fer això hem creat un fitxer .htaccess amb les directives per poder fer login a la carpeta admin.

A la carpeta on es troba l'usuari i contrasenya del login, també hi ha un fitxer .htaccess amb una directiva que impedeix que ningú pugui visualitzar el fitxer des del navegador introduint la URL del fitxer.

Per finalitzar, perquè des del navegador es pugui visualitzar, se li ha de donar permisos al directori, per fer-ho fem: chmod -R 755 cinema/