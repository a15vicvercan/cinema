<?php 
    //funcion para validar las entradas numericas
    function validaEntrada($entrada) {
        return htmlspecialchars(stripslashes(trim($entrada)));
    }

    function validaEntradaNum($entrada) {
        if (!is_numeric($entrada)) {
            return false;
        }
        return true;
    }

    function validaTelefono($entrada) {
        $expresion = "/^[9|6|7][0-9]{8}$/";
        return preg_match($expresion, $entrada);
    }

    //funcion para validar el email
    function validaEmail($email) {
        if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
            return false;
        }
        return true;
    }