<?php
	session_start();
	/*ini_set('display_errors',1);
	ini_set('display_startup_errors',1);
	error_reporting(-1); */ //Errores debugacion

	require_once '../html/Email/fpdf/fpdf.php';
	require_once "../html/Email/PHPMailer_v5.1/class.phpmailer.php";
	require_once 'validar.php';
	require_once 'login.php';

	define('EURO',chr(128)); //Constante con el valor € para el pdf

	//Generamos datos del mail / Configuración
	$mail = new PHPMailer();
	$mail->Mailer="smtp";
	$mail->Helo = "www.gmail.com";
	$mail->Host="smtp.gmail.com";
	$mail->Port=587;
	$mail->Username="cinedawiam@gmail.com";
	$mail->Password="ausias1234";
	$mail->From="cinedawiam@gmail.com";
	$mail->FromName="I AM CINEMA";
	$mail->Timeout=60;
	$mail->IsHTML(true);
	//$mail->SMTPDebug = 2;
	$mail->SMTPSecure = 'tls';
	$mail->IsSMTP();                  // tell the class to use SMTP
	$mail->SMTPAuth = true; 

	//Obtenemos el mail, la sesion, la butaca y el precio
	$emailUsuario = $_SESSION['emailUsuario'];
	$fechaSesion = $_SESSION['fechaSesionSeleccionada'];
	$butacasPrecioSeleccionadas = $_SESSION['butacasPrecioSeleccionadas'];
    $nomUsuari = $_POST['nom'];
    $cognomUsuari = $_POST['cognom'];
	$telefonUsuari = $_POST['telefon'];
	
	//Si los campos del formulario estan vacios, mostramos error
	if (empty($nomUsuari) || empty($cognomUsuari) || empty($telefonUsuari)) {
		header('Location: ../html/Errores/errorDatosFormFinalCompra.php');
		exit;
	}
	
	//si estan llenos los validamos
	$nomUsuari = validaEntrada($nomUsuari);
	$cognomUsuari = validaEntrada($cognomUsuari);
	if (!validaEntradaNum($telefonUsuari)) {
		header('Location: ../html/Errores/errorTelefonoFormFinalCompra.php');
		exit;
	}
	
	$numeroTelefonValid = validaTelefono($telefonUsuari);

	if (!$numeroTelefonValid) {
		header('Location: ../html/Errores/errorTelefonoFormFinalCompra.php');
		exit;
	}
	
	/**** OBTENCION DE TODOS LOS DATOS NECESARIOS ****/
	
    $db_server = mysqli_connect($db_hostname, $db_username, $db_password, $db_database);
    if (!$db_server) die("Unable to connect to MySQL: " . mysql_error());
	mysqli_select_db($db_server, $db_database) or die("Unable to select database: " . mysqli_error());
	$db_server->set_charset("utf8"); //Lo usamos para que la conexión a la BD use utf8
	
	//Conseguimos el id de la sesion
	$query = "SELECT idSessio from SESSIO where (diaSessio = '$fechaSesion')";
	$result = mysqli_query($db_server, $query);
	$rows = mysqli_num_rows($result);
	//En caso que se devuelva un registro vacío, debemos indicarlo como error
	if (!$rows) {
		header('Location: ../html/Errores/errorGestionCompraSessio.php');
		exit;
	}

	//Como solo ha de devolver una sala
	$IDSessio = mysqli_fetch_row($result);
	$IDSessio = $IDSessio[0];

	//Conseguimos el id de la sesion
	$query = "SELECT salaSessio from SESSIO where (diaSessio = '$fechaSesion')";
	$result = mysqli_query($db_server, $query);
	$rows = mysqli_num_rows($result);
	//En caso que se devuelva un registro vacío, debemos indicarlo como error
	if (!$rows) {
		die("Error, no se ha devuelto salaSessio");
		//Pagina error + exit;
		header('Location: ../html/Errores/errorGestionCompraSala.php');
		exit;
	}

	//Como solo ha de devolver un idSessio
	$SalaSessio = mysqli_fetch_row($result);
	$SalaSessio = $SalaSessio[0];

	//Seleccionamos el titulo de la pelicula para la sesión seleccion
	$query = "SELECT titolPeli from SESSIO where (diaSessio = '$fechaSesion')";
	$result = mysqli_query($db_server, $query);
	$rows = mysqli_num_rows($result);
	//En caso que se devuelva un registro vacío, debemos indicarlo como error
	if (!$rows) {
		die("Error, no se ha devuelto pelicula");
	}

	//Como solo ha de devolver un titulo
	$peliSessio = mysqli_fetch_row($result);
	$peliSessio = $peliSessio[0];

	//Obtenemos el horario de la sesion seleccionada
	$query = "SELECT horaSessio from SESSIO where (diaSessio = '$fechaSesion')";
	$result = mysqli_query($db_server, $query);
	$rows = mysqli_num_rows($result);
	//En caso que se devuelva un registro vacío, debemos indicarlo como error
	if (!$rows) {
		die("Error, no se ha devuelto hora");
	}
	//En caso de éxito, nos guardamos el valor devuelto
	$horaSesion = mysqli_fetch_row($result);
	$horaSesion = $horaSesion[0];

	//iniciamos la transaccion para controlar que se meten solo las butacas si no estan asignadas antes de hacer el insert
	$db_server->begin_transaction();

	//Clase PDF DE FPDF para generar el pdf
	class PDF extends FPDF
	{
		// Cabecera de página
		function Header()
		{
			// Logo
			$this->Image('../img/Logos_Cinemes/IAM_CAT_logos_transparent_rgb_3.png',170,8,33);
			// Arial bold 15
			$this->SetFont('Arial','B',15);
			// Movernos a la derecha
			$this->Cell(80);
			// Título
			$this->Image('../img/titolCinema.png',80,15,50);
			// Salto de línea
			$this->Ln(20);
		
		}

		// Pie de página
		function Footer()
		{
			// Posición: a 1,5 cm del final
			$this->SetY(-15);
			// Arial italic 8
			$this->SetFont('Arial','I',8);
			// Número de página
			$this->Cell(0,10,'Page '.$this->PageNo(),0,0,'C');
		}

	}

	//Iremos creando el pdf
	$pdf = new PDF();
	$pdf->AddPage();
	$pdf->Cell(40,10,'Comprovant de Compra');
	$pdf->Ln(10);
	// Arial 12
	$pdf->SetFont('Arial','',12);
	// Background color
	$pdf->SetFillColor(255,175,77);
	// Title
	$pdf->Cell(0,6,utf8_decode("Sessió $fechaSesion, Hora: $horaSesion"),0,1,'L',true);
	// Line break
	$pdf->Ln(5);

	$pdf->Cell(40,10,utf8_decode('Pel·lícula:'));
	$pdf->Cell(40,10,utf8_decode($peliSessio));
	$pdf->Ln(8);

	$pdf->Cell(40,10,utf8_decode('Sala:'));
	$pdf->Cell(40,10,utf8_decode($SalaSessio));
	$pdf->Ln(8);

	$pdf->Cell(40,10,utf8_decode('Client:'));
	$pdf->Cell(40,10,utf8_decode($nomUsuari . ' ' . $cognomUsuari));
	$pdf->Ln(20);
	
	//Nos aseguramos de que el usuario no existe
	$query = "SELECT * from USUARI where email = '$emailUsuario'";
	$result = mysqli_query($db_server, $query);
	$rows = mysqli_num_rows($result);
	//En caso que se devuelva un registro vacío, significa que podemos hacer todos los insert del usuario
	if (!$rows) {
		//Metemos al usuario
		$query = "INSERT INTO USUARI (nomUsuari, cognomUsuari, telefon, email) VALUES ('$nomUsuari', '$cognomUsuari', '$telefonUsuari', '$emailUsuario')";
		$result = mysqli_query($db_server, $query);
	}
	//En caso que se devuelva un registro vacío, debemos indicarlo como error
	if (!$result) {
		header('Location: ../html/Errores/errorGestionCompraInsertUsuario.php');
		exit;
	}
	else {
		
		//Una vez validados los datos, miramos si las butacas seleccionadas estan aun libres
		//Primero tratamos las butacas sacando el precio y su numero de butaca
		$errorCompra = false;
		$butacasPrecioSeleccionadas = explode('#', $butacasPrecioSeleccionadas);

		$tamañoButacasPrecioSeleccionadas = count($butacasPrecioSeleccionadas);

		$numeroEntradesCompradesUsuari = 0;
		$precioTotalCompra = 0;

		for ($i = 0; $i < $tamañoButacasPrecioSeleccionadas && !$errorCompra; $i +=2) {
			//Si no llega al ultimo, que es vacio, hacemos el insert
			if ($i != $tamañoButacasPrecioSeleccionadas-1) {

				//Obtenemos el par numButaca, precio
				$numButaca = $butacasPrecioSeleccionadas[$i];
				$preuButaca = $butacasPrecioSeleccionadas[$i+1];
			
				//Miramos si la butaca existe en la BD
				$query = "SELECT * from ENTRADA where numButaca = '$numButaca' and idSessio = '$IDSessio'";
				$result = mysqli_query($db_server, $query);
				$rows = mysqli_num_rows($result);
				//En caso que se devuelva un registro vacío, debemos hacer el insert de las entradas ya que la butaca esta vacia
				if (!$rows) {
					//Generamos la query de insert entrada
					++$numeroEntradesCompradesUsuari;
					$precioTotalCompra += $preuButaca;
					$query = "INSERT INTO ENTRADA (idEntrada, preu, numButaca, idSessio, email, numSala) VALUES (null, '$preuButaca', '$numButaca', '$IDSessio', '$emailUsuario', '$SalaSessio')"; //idEtrada AUTOINCREMENT
					$result = mysqli_query($db_server, $query);

					//Generamos la informacion de las entradas para el pdf
					$msgPDFandMAIL = "Entrada número #$numeroEntradesCompradesUsuari, butaca $numButaca x $preuButaca";
					$pdf->Cell(0,10,utf8_decode($msgPDFandMAIL) . EURO,0,1);
				}
				//En caso que se devuelva un registro, debemos indicarlo como error
				else {
					$errorCompra = true;
				}
			}
		}
		//Si todo acaba con exito, guardamos los cambios
		if (!$errorCompra){
			$db_server->commit();
			mysqli_close($db_server);

			//Guardamos el pdf en una variable
			$pdf->Ln(20);
			$msgPDFandMAIL = "Total pagat = $precioTotalCompra";
			$pdf->Cell(0,10,utf8_decode($msgPDFandMAIL) . EURO,0,1);
			$archivoPDF = $pdf->Output('', 'S');

			//Enviar Mail
			//Enviamos el correo, al mail del usuario en concreto
			$mail->AddAddress($emailUsuario);
			$mail->Subject='Resguard compra d\'entrades pel dia ' . $fechaSesion;
			$mail->Body = "Hola $emailUsuario!<br>Aquí tens el resguard de les teves entrades.<br>Gràcies per comprar al teu cinema!<br>I AM CINEMA!";
			//Añadimos el archivo al mail
			$mail->AddStringAttachment($archivoPDF,'entrades' . $fechaSesion . '.pdf','base64');
			if ($mail->Send()) {
				header('Location: ../html/Errores/correctaTransaccioEntrada.php');
			}
			else {
				echo "<br>Correo no generado correctamente.<br>";
			}
			//Borramos las sesiones
			unset($_SESSION['emailUsuario']);
			unset($_SESSION['fechaSesionSeleccionada']);
			unset($_SESSION['butacasPrecioSeleccionadas']);
		}
		//En caso de que devuelva un error, deshacemos todo lo anterior
		else {
			$db_server->rollback();
			mysqli_close($db_server);
			header('Location: ../html/Errores/errorTransaccioEntrada.php');
		} 
	}

?>