<?php
session_start();

require_once 'validar.php';

$emailUsuario = $_POST['mail'];

//Obtenemos la fecha del dia actual
$fechaDiaActual = getdate();
$fechaDiaActual = $fechaDiaActual['year'] . '-' . $fechaDiaActual['mon'] . '-' . $fechaDiaActual['mday'];

//Validamos usuario conforme sea un mail correcto
if (!validaEmail($emailUsuario)) 
{
    header('Location: ../html/Errores/errorMail.php');
    exit;
}
//email válido, miramos si esta o no en bd
else 
{
    require_once 'login.php';
    $db_server = mysqli_connect($db_hostname, $db_username, $db_password, $db_database);
    if (!$db_server) die("Unable to connect to MySQL: " . mysql_error());
    mysqli_select_db($db_server, $db_database) or die("Unable to select database: " . mysqli_error());
	$db_server->set_charset("utf8"); //Lo usamos para que la conexión a la BD use utf8
    
    //Comprovamos si esta o no el email en BD
    $query = "SELECT email from USUARI where (email = '$emailUsuario')";
    $result = mysqli_query($db_server, $query);
    $rows = mysqli_num_rows($result);
    
    //Guardamos el mail en la sesion
    $_SESSION['emailUsuario'] = $emailUsuario;

    $usuarioNoRegistradoBD = false;
    //En caso de error, indicamos que el usuario no existe
    if(!$rows) {
        $usuarioNoRegistradoBD = true;
    }
    //Caso que el usuario existe en BD
    else {
        //Comprovamos si el usuario tiene entradas futuras
        $query = "SELECT count(*) from ENTRADA where email = '$emailUsuario' and idSessio IN (select idSessio from SESSIO where diaSessio > '$fechaDiaActual')";
        $result = mysqli_query($db_server, $query);
        $rowsFuturas = mysqli_num_rows($result);

        //Comprovamos si el usuario tiene entradas pasadas
        $query = "SELECT count(*) from ENTRADA where email = '$emailUsuario' and idSessio IN (select idSessio from SESSIO where diaSessio < '$fechaDiaActual')";
        $result = mysqli_query($db_server, $query);
        $rowsPasadas = mysqli_num_rows($result);
    }
    
    $sessioSeleccionada = $_SESSION['seleccioSessio'];
    $comprovaEntrades = $_SESSION['comprovaEntrades'];

    //Si el mail no existe en BD, lo debemos guardar para posteriormente no tener que pedirselo al usuario otra vez
    
    //Un usuario que estaba en BD tambien puede comprar si las entradas futuras ya han sido usadas
    if (!$rowsFuturas && $comprovaEntrades){
        header('Location: ../html/Errores/errorNoEntradasFuturas.php');
    }
    else if ($usuarioNoRegistradoBD || (!$usuarioNoRegistradoBD && !$rowsFuturas && $rowsPasadas)){
        //Proceso Comprar entradas

        //Informaremos de la sesion, guardando el mail en la session
        if($sessioSeleccionada != "true"){
            header('Location: ../html/Usuari/infoSessioSeleccionada.php');
        }
        else {
            header('Location: ../html/Usuari/butacas.php');
        }
    }
    else 
    {
        //Si esta en BD mostramos las entradas futuras que tiene
        header('Location: ../html/Usuari/mostrarCompra.php');
    }

    unset($_SESSION['seleccioSessio']);
    unset($_SESSION['comprovaEntrades']);

    mysqli_close($db_server);
}
