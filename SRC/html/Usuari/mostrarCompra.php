<?php 
	session_start();
	
	/**** TRATAMIENTO DE LOS DATOS ****/

	$emailUsuario = $_SESSION['emailUsuario'];
	//Obtencion de la fecha actual
	$fechaDiaActual = getdate();
	$fechaDiaActual = $fechaDiaActual['year'] . '-' . $fechaDiaActual['mon'] . '-' . $fechaDiaActual['mday'];
	
	require_once '../../php/login.php';

	$db_server = mysqli_connect($db_hostname, $db_username, $db_password, $db_database);
	if (!$db_server) die("Unable to connect to MySQL: " . mysql_error());    //imprimeeix a msg i acaba l'script
	mysqli_select_db($db_server, $db_database) or die("Unable to select database: " . mysqli_error());
	$db_server->set_charset("utf8"); //Lo usamos para que la conexión a la BD use utf8
	
	/**
	 * buscamos las entradas que tiene el usuario ya compradas
	 */

	//Guardamos el dia de la sesion
	$query = "SELECT diaSessio FROM SESSIO WHERE idSessio IN (SELECT idSessio FROM ENTRADA WHERE email = '$emailUsuario')";
	$result = mysqli_query($db_server, $query);
	$rows = mysqli_num_rows($result);
	//En caso que se devuelva un registro vacío, debemos indicarlo como error
	if (!$rows) {
		header('Location: ../Errores/errorUsuariSeleccionDiaSessio.php');
        exit;
	}
	//En caso de éxito, nos guardamos el valor devuelto
	$diaSessioBD = mysqli_fetch_row($result);
	$diaSessioBD = $diaSessioBD[0];

	//Obtenemos el nombre del usuario
	$query = "SELECT nomUsuari FROM USUARI WHERE email = '$emailUsuario'";
	$result = mysqli_query($db_server, $query);
	$rows = mysqli_num_rows($result);
	//En caso de éxito, nos guardamos el valor devuelto
	$nomUsuari = mysqli_fetch_row($result);
	$nomUsuari = $nomUsuari[0];

	//Obtenemos la informacion de la entrada
	$query = "SELECT * FROM ENTRADA WHERE email = '$emailUsuario' AND idSessio IN (SELECT idSessio FROM SESSIO WHERE diaSessio >= '$fechaDiaActual')";
	$result = mysqli_query($db_server, $query);
	if (!$result) die ("Database access failed: " . mysql_error());
	$rows = mysqli_num_rows($result);

	$tieneEntradas = true;
	if (!$rows) {
		$tieneEntradas = false;
	}

	/**** GENERAMOS LA TABLA QUE MOSTRARA LAS ENTRADAS FUTURAS DEL USUARIO ****/

	$taulaEntrades = "<table>";
	$taulaEntrades .= "<tr><th class='centro'>Número Entrada</th><th class='centro'>Preu</th><th class='centro'>Butaca</th><th class='centro'>Sessió</th><th class='centro'>Usuari</th><th class='centro'>Sala</th></tr>";
	
	$mensajeUsuario = "<p>$nomUsuari, lamentem que no puguis comprar més entrades. Vosté encara en té aquestes pendents:</p>";

	while($fila = mysqli_fetch_row($result)) {
		$taulaEntrades .= "<tr>";
		for($i = 0; $i < count($fila); $i++){
			$taulaEntrades .= "<td>";
			if ($i != 3) {
				$taulaEntrades .= $fila[$i];
			}
			else {
				$taulaEntrades .= $diaSessioBD;
			}
			$taulaEntrades .= "</td>";
		}
		$taulaEntrades .= "</tr>";
	}
	$taulaEntrades .= "</table>";
	mysqli_close($db_server);
?>


<!DOCTYPE html>

<html lang='ca'>

<head>
	<meta charset="UTF-8">
	<title>I AM CINEMA</title>
	<link rel="stylesheet" href="../../css/reset.css">
	<link rel="stylesheet" href="../../css/normalize.css">
	<link rel="stylesheet" href="../../css/skeleton.css">	
	<link rel=stylesheet href="../../css/style.css">
	<script src="../../js/jquery-3.3.1.min.js"></script>
	<script src="../../js/funciones.js"></script>
	<link rel="icon" type="image/png" href="../../img/icon.png">
</head>

<body>
	<?php include("../Includes/header.php"); ?>

	<div class="content">
		
		<h1>Mostrar compra</h1>

		<?php
			if ($tieneEntradas) {
				echo $mensajeUsuario;
				echo $taulaEntrades;
			}
			else {
				header('Location: infoSessioSeleccionada.php');
				exit;
			}
		?>

		<a class="button botonFooter" href="../../index.php">Inici</a>

	</div>

	<?php include("../Includes/footer.php"); ?>
</body>

</html>