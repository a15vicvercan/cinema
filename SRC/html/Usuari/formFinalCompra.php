<?php
    session_start();

    $butacas = $_POST['butacasSeleccionadas'];
    if ($butacas == "") {
        //Error no hay butacas seleccionadas
        header('Location: ../Errores/errorSeleccionaButacas.php');
        exit;
    }

    //Guardamos las butacas seleccionadas
    $_SESSION['butacasPrecioSeleccionadas'] = $butacas;

?>


<!DOCTYPE html>

<html lang='ca'>

<head>
    <meta charset="UTF-8">
    <title>I AM CINEMA</title>
    <link rel="stylesheet" href="../../css/reset.css">
    <link rel="stylesheet" href="../../css/normalize.css">
    <link rel="stylesheet" href="../../css/skeleton.css">
    <link rel=stylesheet href="../../css/style.css">
    <link rel=stylesheet href="../../css/styleForm.css">

    <script src="../../js/jquery-3.3.1.min.js"></script>

    <link rel="icon" type="image/png" href="../../img/icon.png">
</head>

<body>
    <?php include("../Includes/header.php"); ?>

    <div class="content">

        <?php
        //Data recibida por el campo hidden al seleccionar la fecha
            /*$data = $_POST['data'];
            echo $data . "<br>";*/
        ?>

        <h1>Abans de finalitzar la teva compra...</h1>

        <p></p>

        <form action="../../php/gestioCompra.php" method="post">
            <div>
                <h6>Introdueix el teu nom</h6>
            </div>
            <div>
                <label for="nom">Nom:</label>
                <input type="text" id="nom" name="nom" />
            </div>
            <div>
                <h6>Introdueix el teu cognom</h6>
            </div>
            <div>
                <label for="cognom">Cognom:</label>
                <input type="text" id="cognom" name="cognom" />
            </div>
            <div>
                <h6>Introdueix el teu telèfon</h6>
            </div>
            <div>
                <label for="telefon">Telèfon:</label>
                <input type="text" id="telefon" name="telefon" />
            </div>
            
            <button type="submit"  class='botonFooterSEG'>Comprar Entrades</button>
        </form>

    </div>

    <?php include("../Includes/footer.php"); ?>
</body>

</html>