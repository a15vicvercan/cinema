<?php
	session_start();
	require_once '../../php/login.php';
	$fechaSessio = $_SESSION['fechaSesionSeleccionada'];
	
	/**** GENERAMOS PATIO BUTACAS ****/
	/*Primero debemos de buscar si en BD existen ya butacas asignadas a esa sesión 
	* En caso de que las haya, las pintamos como corresponden
	* Posteriormente pintamos como corresponden las butacas de la sala
	*/

    $db_server = mysqli_connect($db_hostname, $db_username, $db_password, $db_database);
    if (!$db_server) die("Unable to connect to MySQL: " . mysql_error());
	mysqli_select_db($db_server, $db_database) or die("Unable to select database: " . mysqli_error());
	$db_server->set_charset("utf8"); //Lo usamos para que la conexión a la BD use utf8

	/*
	* Debemos identificar si la sesión será normal o especial (Dia espectador) para poder usar
	* los precios correspondientes
	*/
	$query = "SELECT sessioEspecial from SESSIO where (diaSessio = '$fechaSessio')";
	$result = mysqli_query($db_server, $query);
	$rows = mysqli_num_rows($result);
	//En caso que se devuelva un registro vacío, debemos indicarlo como error
	if (!$rows) {
		header('Location: ../Errores/errorUsuariConsultaSessio.php');
		exit;
	}
	//En caso de éxito, nos guardamos el valor devuelto
	$esSessioEspecial = mysqli_fetch_row($result);
	$esSessioEspecial = $esSessioEspecial[0];
	

	/*
	* Debemos identificar si la sesión asociada a la fecha seleccionada
	* contiene butacas VIP o no
	*/
	$query = "SELECT sessioVip from SESSIO where (diaSessio = '$fechaSessio')";
	$result = mysqli_query($db_server, $query);
	$rows = mysqli_num_rows($result);
	//En caso que se devuelva un registro vacío, debemos indicarlo como error
	if (!$rows) {
		header('Location: ../Errores/errorUsuariConsultaSessio.php');
		exit;
	}
	//En caso de éxito, nos guardamos el valor devuelto
	$esSessioVip = mysqli_fetch_row($result);
	$esSessioVip = $esSessioVip[0];


	/*
	* Debemos obtener las butacas, en caso que las tenga, asociadas ya a esa sesión,
	* es decir, las butacas que ya estan ocupadas en esa sala.
	*/
	$subquery = "SELECT idSessio from SESSIO where (diaSessio = '$fechaSessio')";
	$query = "SELECT numButaca from ENTRADA where (idSessio = ($subquery))";
	$result = mysqli_query($db_server, $query);
	$rows = mysqli_num_rows($result);
	
	//Controlamos si el patio de butacas esta vacío o ya tiene alguna de estas asignada
	$patioButacasVacio = false;
	if (!$rows) {
		$patioButacasVacio = true;
	}
	
	//Guardamos los registros devueltos en forma de vector para poder acceder mejor a ellos después
	$butacasOcupadasBD = array();
	for ($i = 0; $i < $rows; $i++) {
		$consulta = mysqli_fetch_assoc($result);
		foreach ($consulta as $key => $valor) {
			array_push($butacasOcupadasBD, $valor);
		}
	}

	//Nos creamos unas variables para controlar, assignar y posteriormente, si se requiere, modificarlas facilmente tanto el precio
	//como la correspondiente fila VIP
	$preuButacaNoVip = 6;
	$preuButacaVip = 8;
	$preuButacaNoVipEspepcial = 4;
	$preuButacaVipEspepcial = 6;
	$filaVip = 6;

	/*
	* Generaremos el patio de butacas, 12 filas por 10 columnas, mediante los datos obtenidos anteriormente
	*/
	$patioButacas = '<img id="pantalla" src="../../img/butaques/pantalla.png"><table>';
	for ($i = 1; $i <= 12; $i++) {
		/*
		* Como indica el enunciado, las filas van de A-L, así que las debemos convertir
		*/
		switch($i) {
			case 1:
				$letraFila = 'A';
				break;
			case 2:
				$letraFila = 'B';
				break;
			case 3:
				$letraFila = 'C';
				break;
			case 4:
				$letraFila = 'D';
				break;
			case 5:
				$letraFila = 'E';
				break;
			case 6:
				$letraFila = 'F';
				break;
			case 7:
				$letraFila = 'G';
				break;
			case 8:
				$letraFila = 'H';
				break;
			case 9:
				$letraFila = 'I';
				break;
			case 10:
				$letraFila = 'J';
				break;
			case 11:
				$letraFila = 'K';
				break;
			case 12:
				$letraFila = 'L';
				break;
		}

		/*
		* Ahora, rellenaremos el patio de butacas con la correspondiente imagen de la butaca, segun sea ocupada, seleccionada o libre
		*/
		$patioButacas .= '<tr>';
		for ($j = 1; $j <= 10; $j++) {
			//Guardamos el codigo/numero de butaca que usaremos para identificarla
			$numButuacaActual = $letraFila . $j;
			$butacaOcupadaEncontrada = false;

			//Primero buscamos las butacas ocupadas y las tratamos con su imagen correspondiente
			for($k = 0; $k < $rows && !$butacaOcupadaEncontrada; $k++) {
				if (!$patioButacasVacio && ($numButuacaActual == $butacasOcupadasBD[$k])) {
					//Si hay alguna butaca en el vector que devuelve la consulta, pintamos ocupadas
					$patioButacas .= '<td class="ocupat" numButaca="' . $numButuacaActual . '"></td>';
					$butacaOcupadaEncontrada = true;
				}
			}

			//En caso de que no encontremos ninguna butaca ocupada, miramos de que tipo de las que quedan es
			if (!$butacaOcupadaEncontrada) {
				//Si la sesion es una normal, los precios seran de 6 y 8 € (normales, vips)
				if (!$esSessioEspecial) {
					if (($i == 1 && ($j == 1 || $j == 2))) {
						//Butacas para personas con dificultades fisicas
						$patioButacas .= '<td class="minu" numButaca="' . $numButuacaActual . '" preu = "'. $preuButacaNoVip . '"></td>';
					}
					else if ($i == $filaVip && $esSessioVip) {
						//Gestion butacas vip
						$patioButacas .= '<td class="lliure VIP" numButaca="' . $numButuacaActual . '" preu = "'. $preuButacaVip . '"></td>';
					}
					else {
						//Butacas libres
						$patioButacas .= '<td class="lliure" numButaca="' . $numButuacaActual . '"  preu = "'. $preuButacaNoVip . '"></td>';
					}
				}
				//En caso contrario, se trata de una sesion del dia del espectador, los precios seran de 4 y 6 €
				else {
					
					if (($i == 1 && ($j == 1 || $j == 2))) {
						//Butacas para personas con dificultades fisicas
						$patioButacas .= '<td class="minu" numButaca="' . $numButuacaActual . '" preu = "'. $preuButacaNoVipEspepcial . '"></td>';
					}
					else if ($i == $filaVip && $esSessioVip) {
						//Gestion PROVISIONAL butacas vip
						$patioButacas .= '<td class="lliure VIP" numButaca="' . $numButuacaActual . '" preu = "'. $preuButacaVipEspepcial . '"></td>';
					}
					else {
						//Butacas libres
						$patioButacas .= '<td class="lliure" numButaca="' . $numButuacaActual . '"  preu = "'. $preuButacaNoVipEspepcial . '"></td>';
					}
				}
			}
		}
		$patioButacas .= '</tr>';
	}
	$patioButacas .= '</table>';
	mysqli_close($db_server);
?>


<!DOCTYPE html>

<html lang='ca'>

<head>
	<meta charset="UTF-8">
	<title>I AM CINEMA</title>
	<link rel="stylesheet" href="../../css/reset.css">
	<link rel="stylesheet" href="../../css/normalize.css">
  	<link rel="stylesheet" href="../../css/skeleton.css">
	<link rel=stylesheet href="../../css/style.css">
	<link rel=stylesheet href="../../css/styleButaques.css">
	<script src="../../js/jquery-3.3.1.min.js"></script>
	
	<script src="../../js/funcionesButaques.js"></script>
	<script src="../../js/sweetalert2.all.min.js"></script>
	<link rel="icon" type="image/png" href="../../img/icon.png">
</head>

<body>
	<?php include("../Includes/header.php"); ?>

	<div class="content">
		<div class="container">
		<form action='formFinalCompra.php' class = "formulariFinalCompra" method='post'>
			<div class="row">
					<div class="eight columns" id="content">		
						<?php			  
							echo $patioButacas;
						?>
					</div>
					<div class="four columns">
						<div id="total">
							<pre>Preu <?php if ($esSessioEspecial) echo "del dia de l'espectador"; ?></pre><br>
							<p id="preu">0 €</p>
						</div>
						<div id="panel"><pre>Historial</pre><br></div>
					</div>
				</div>
				<input type='submit' name='siguienteFormFinalCompra' value = 'Següent'  class='button botonFooterSEG'>
			</form>
		</div>
	</div>

	<?php include("../Includes/footer.php"); ?>
</body>

</html>