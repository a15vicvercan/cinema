<?php
    session_start();

    /**
     * Debemos mirar si la sesion que se recibe esta asignada
     */
    if(isset($_POST['seleccioSessio'])){
        $fecha = $_POST['data'];
        $seleccioSessio = $_POST['seleccioSessio'];
        $_SESSION['seleccioSessio'] = "true";

        require_once '../../php/login.php';
        
        $db_server = mysqli_connect($db_hostname, $db_username, $db_password, $db_database);
        if (!$db_server) die("Unable to connect to MySQL: " . mysql_error());
        mysqli_select_db($db_server, $db_database) or die("Unable to select database: " . mysqli_error());
	    $db_server->set_charset("utf8"); //Lo usamos para que la conexión a la BD use utf8
        
        //Miramos si esta o no la sesion en BD
        $query = "SELECT * from SESSIO where diaSessio = '$fecha'";
        $result = mysqli_query($db_server, $query);
        $rows = mysqli_num_rows($result);
        if (!$rows) {
            //Error no hay sesion en esa fecha
            header('Location: ../Errores/errorSessioSeleccionada.php');
            exit;
        }

        //Creamos la sesion de fecha para ir usandola despues
        $_SESSION['fechaSesionSeleccionada'] = $fecha;
        mysqli_close($db_server);

    }
    //Si venimos por el lado del calendario
    else if(isset($_POST['data'])) {
        
        /**** TRATAMIENTO DE LA INFORMACION POR FECHA ****/
        $fecha = $_POST['data'];
        $fecha = explode('#', $fecha);

        //nomes tenim una data seleccionada
        foreach ($fecha as $clave => $valor) {
            switch($clave) {
                case 0:
                    //Obtenemos el dia de la fecha
                    $dia = $valor;
                    break;
                case 1:
                    //Obtenemos el mes de la fecha
                    $mes = $valor;
                    break;
                case 2:
                    //Obtenemos el año de la fecha
                    $any = $valor;
                    break;
            }
        }

        //Una vegada tenim la data tractada, busquem si esta o no a BD
        $fecha = $any . '-' . $mes . '-' . $dia;

        require_once '../../php/login.php';
        
        $db_server = mysqli_connect($db_hostname, $db_username, $db_password, $db_database);
        if (!$db_server) die("Unable to connect to MySQL: " . mysql_error());
        mysqli_select_db($db_server, $db_database) or die("Unable to select database: " . mysqli_error());
	    $db_server->set_charset("utf8"); //Lo usamos para que la conexión a la BD use utf8
        
        //Miramos si esta o no la sesion en BD
        $query = "SELECT * from SESSIO where diaSessio = '$fecha'";
        $result = mysqli_query($db_server, $query);
        $rows = mysqli_num_rows($result);
        //En caso que se devuelva un registro vacío, debemos indicarlo como error
        if (!$rows) {
            //Error no hay sesion en esa fecha
            header('Location: ../Errores/errorSessioSeleccionada.php');
            exit;
        }

        //En caso de éxito, nos guardamos el valor devuelto
        //Creamos la sesion de fecha para ir usandola despues
        $_SESSION['fechaSesionSeleccionada'] = $fecha;
        mysqli_close($db_server);
    }
    else {
        $_SESSION['comprovaEntrades'] = "true";
    }

?>


<!DOCTYPE html>

<html lang='ca'>

<head>
    <meta charset="UTF-8">
    <title>I AM CINEMA</title>
    <link rel="stylesheet" href="../../css/reset.css">
    <link rel="stylesheet" href="../../css/normalize.css">
    <link rel="stylesheet" href="../../css/skeleton.css">
    <link rel=stylesheet href="../../css/style.css">
    <link rel=stylesheet href="../../css/styleForm.css">

    <script src="../../js/jquery-3.3.1.min.js"></script>
    <script src="../../js/funcionesMail.js"></script>

    <link rel="icon" type="image/png" href="../../img/icon.png">
</head>

<body>
    <?php include("../Includes/header.php"); ?>

    <div class="content">

        <h1>Login</h1>
        
        <form action="../../php/gestioUsuari.php" method="post">
            <div>
                <h4>Introdueix el teu mail</h4>
            </div>
            <div>
                <label for="mail">Email:</label>
                <input type="text" id="mail" name="mail" />
            </div>

            <button type="submit">Següent</button>


        </form>

    </div>

    <?php include("../Includes/footer.php"); ?>
</body>

</html>