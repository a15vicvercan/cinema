<?php
	require_once '../../php/login.php';

	$mostrarPeliculaDataSessio = false;
	$titulo = $_POST['peliculaSeleccionada'];

	//Obtenemos la fecha del dia actual
	$fechaDiaActual = getdate();
	$fechaDiaActual = $fechaDiaActual['year'] . '-' . $fechaDiaActual['mon'] . '-' . $fechaDiaActual['mday'];

	/**
	 * Dependiendo del camino por el que vengas, mostraremos diferentes cosas.
	 * Si vienes por el camino de la cartelera, se muestra un resumen de las sesiones futuras que tiene esta y luego el posterior tratamiento de la compra
	 * Si vienes por el camino del calendario, mostraremos directamente el tratamiento de la compra
	 */
	//Camino Calendario
	if(empty($titulo)){

		$mostrarPeliculaDataSessio = true;
		session_start();

		//Obtenemos la fecha de la sesion 
		$fecha = $_SESSION['fechaSesionSeleccionada'];
		$fechaTitulo = explode('-', $fecha);

		/**** Tratamiento/Manipulación de la fecha para poder usarla en BD ****/

		foreach ($fechaTitulo as $clave => $valor) {
			switch($clave) {
				case 0:
					//Obtenemos el año de la fecha
					$any = $valor;
					break;
				case 1:
					//Obtenemos el mes de la fecha
					$mes = $valor;
					break;
				case 2:
					//Obtenemos el dia de la fecha
					$dia = $valor;
					break;
			}
		}
		
		$db_server = mysqli_connect($db_hostname, $db_username, $db_password, $db_database);
		if (!$db_server) die("Unable to connect to MySQL: " . mysql_error());
		$db_server->set_charset("utf8"); //Lo usamos para que la conexión a la BD use utf8
		mysqli_select_db($db_server, $db_database) or die("Unable to select database: " . mysqli_error());
		
		
		//Una vez tenemos la fecha, seleccionamos la pelicula que tiene esa sesion
		$subquery = "SELECT titolPeli from SESSIO where (diaSessio = '$fecha')";
		$result = mysqli_query($db_server, $subquery);
		$rows = mysqli_num_rows($result);
		//Nos quedamos con el titulo de la pelicula
		$titulo = mysqli_fetch_row($result);
		$titulo = $titulo[0];

		//Obtenemos toda la infromacion de la pelicula de esa sesion
		$query = "SELECT * from PELICULA where (titol = ($subquery))";
		$result = mysqli_query($db_server, $query);
		$rows = mysqli_num_rows($result);
	}
	//Camino Cartelera
	else {
		
		$db_server = mysqli_connect($db_hostname, $db_username, $db_password, $db_database);
		if (!$db_server) die("Unable to connect to MySQL: " . mysql_error());
		mysqli_select_db($db_server, $db_database) or die("Unable to select database: " . mysqli_error());
		$db_server->set_charset("utf8"); //Lo usamos para que la conexión a la BD use utf8

		//Selecion de la informacion pelicula
		$query = "SELECT * from PELICULA where titol = '$titulo'";
		$result = mysqli_query($db_server, $query);
		$rows = mysqli_num_rows($result);

		//Seleccion de la hora y fecha de las sessiones de la pelicula futuras
		$query = "SELECT diaSessio, horaSessio from SESSIO where titolPeli = '$titulo' and diaSessio >= '$fechaDiaActual'";
		$result2 = mysqli_query($db_server, $query);
		$rows2 = mysqli_num_rows($result2);

		//Error en la obtencion del dia y la hora de la sesion
		if(!$rows2) {
			$noSessioFutura = "<p>Aquesta pelicula no te sessions futures.</p>";
		}
	}
	
	//Error en la obtencion de la informacion de la pelicula
	if (!$rows) {
		header('Location: ../Errores/errorSeleccionaPelicula.php');
        exit;
	}

	/**** TRATAMIENTO DE LA INFORMACION DE LA PELICULA ****/

	$consulta = mysqli_fetch_assoc($result);
	$infoPelicula = '<div class = "informacionPelicula">';
	foreach ($consulta as $key => $valor) {
		switch ($key) {
			case 'sinopsi':
				$campoBD = "<div class='textoInfo'><p>" . $valor . "</p>";
				break;
			case 'duracio':
				$campoBD = "<p><strong>Duració</strong>: " . $valor . " min</p>";
				break;
			case 'director':
				$campoBD = "<p><strong>Director</strong>: " . $valor . "</p>";
				break;
			case 'anyEstrena':
				$campoBD = "<p><strong>Any d'estrena</strong>: " . $valor . "</p></div>";
				break;
			case 'cartell':
				$campoBD = "<img src='" . $valor . "' class='CartellPeli cartellInfo' alt='Cartell Pelicula'>";
				break;
		}
		$infoPelicula .= $campoBD;
	}
	$infoPelicula .= '</div>';

	/**** TRATAMIENTO DE LAS SESIONES FUTURAS ****/

	$infoSessions = "<form action='mail.php' method='post'>";
	$infoSessions .= "<p class='textSelSessio'>Selecciona una sessió:</p><table class='sessionsFutures'>";
	$infoSessions .= "<tr><th>Dia</th><th>Hora</th></tr>";
	while($fila = mysqli_fetch_row($result2)) {
		$infoSessions .= "<tr>";
		for($i = 0; $i < count($fila); $i++){
			if($i == 0){
				$infoSessions .= "<td><button class='dataCalendari' data='$fila[$i]' type='submit'>" . $fila[$i] . "</button></td>";
			}
			else {
				$infoSessions .= "<td>" . $fila[$i] . "</td>";
			}
		}
		$infoSessions .= "</tr>";
	}
	$infoSessions .= "</table>";
	$infoSessions .= "</form>";

	$seguent = "<a class='button botonFooterSEG' href='butacas.php'>SEGÜENT</a>";
	mysqli_close($db_server);
?>



<!DOCTYPE html>

<html lang='ca'>

<head>
	<meta charset="UTF-8">
	<title>I AM CINEMA</title>
	<link rel="stylesheet" href="../../css/reset.css">
	<link rel="stylesheet" href="../../css/normalize.css">
  	<link rel="stylesheet" href="../../css/skeleton.css">
	<link rel=stylesheet href="../../css/style.css">
	<link rel=stylesheet href="../../css/styleSessioSeleccionada.css">

	<script src="../../js/jquery-3.3.1.min.js"></script>
	<script src="../../js/funciones.js"></script>
	<script src="../../js/funcionSessioSeleccionada.js"></script>
	<link rel="icon" type="image/png" href="../../img/icon.png">
</head>

<body>
	<?php include("../Includes/header.php"); ?>

	<div class="content">
		<?php 
			if ($mostrarPeliculaDataSessio) {
				echo ('<h1>Informació Sessió  ' . $dia . '/' . $mes . '/' . $any . '</h1>');
				echo "<h2>" . $titulo . "</h2>";
			}
			else {
				echo "<h1>$titulo</h1>";
			}
		?>
        
		<!-- DATOS PELICULA DE ESA SESION -->
		<?php echo $infoPelicula ?>

		<!--MOSTRAR DIA Y HORA SESIONES-->
		<div>
			<?php
				if(!$mostrarPeliculaDataSessio) {
					if(!$rows2) {
						echo $noSessioFutura;
					}
					else {
						echo $infoSessions;
					}
				}
			?>
		</div>

		<a class="button" href="../../index.php">ANTERIOR</a>
		
		<?php
			if($mostrarPeliculaDataSessio) {
				echo $seguent;
			}
		?>

	</div>
		
	<?php include("../Includes/footer.php"); ?>
</body>

</html>