<!DOCTYPE html>

<html lang="ca">

<head>
	<meta charset="UTF-8">
	<title>I AM CINEMA</title>
	<link rel="stylesheet" href="../../css/reset.css">
	<link rel="stylesheet" href="../../css/normalize.css">
  	<link rel="stylesheet" href="../../css/skeleton.css">
	<link rel=stylesheet href="../../css/style.css">
	<link rel="icon" type="image/png" href="../../img/icon.png">
</head>

<body>
	<?php include("../Includes/header.php"); ?>

	<div class="content paginaError">
		
		<h1>Error!</h1>

		<p>No pots deixar cap camp buit.</p>
		
		<a class="button botonFooter" href="../Usuari/butacas.php">Torna a seleccionar les butaques</a>

	</div>

	<?php include("../Includes/footer.php"); ?>
</body>

</html>