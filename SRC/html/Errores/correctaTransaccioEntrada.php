<!DOCTYPE html>

	<html lang='ca'>

	<head>
		<meta charset="UTF-8">
		<title>I AM CINEMA</title>
		<link rel="stylesheet" href="../../css/reset.css">
		<link rel="stylesheet" href="../../css/normalize.css">
		<link rel="stylesheet" href="../../css/skeleton.css">
		<link rel=stylesheet href="../../css/style.css">
		<link rel="icon" type="image/png" href="../../img/icon.png">
	</head>

	<body>
		<?php include("../Includes/header.php"); ?>

		<div class="content paginaError">
			
			<h1>Compra realitzada amb éxit!</h1>

			<p>La teva compra s'ha realitzat correctament.<br>Consulta el teu correu!</p>

			<a class="button botonFooter" href="../../index.php">INICI</a>

		</div>

		<?php include("../Includes/footer.php"); ?>
	</body>

</html>