<?php
	require_once '../../php/login.php';

	$fechaSessio = $_POST['data'];
	$fechaSessio = explode('#', $fechaSessio);

	/**** Tratamiento/Manipulación de la fecha para poder usarla en BD ****/

	foreach ($fechaSessio as $clave => $valor) {
		switch($clave) {
			case 0:
				//Obtenemos el dia de la fecha
				$dia = $valor;
				break;
			case 1:
				//Obtenemos el mes de la fecha
				$mes = $valor;
				break;
			case 2:
				//Obtenemos el año de la fecha
				$any = $valor;
				break;
		}
	}

	//Una vez hemos desglosado la fecha, la adaptamos a como se guarda en BD
	$fechaSessio = $any . '-' . $mes . '-' . $dia;

	
	/**** GENERAMOS PATIO BUTACAS ****/
	
	/*Primero debemos de buscar si en BD existen ya butacas asignadas a esa sesión 
	* En caso de que las haya, las pintamos como corresponden
	* Posteriormente pintamos como corresponden las butacas de la sala
	*/

    $db_server = mysqli_connect($db_hostname, $db_username, $db_password, $db_database);
    if (!$db_server) die("Unable to connect to MySQL: " . mysql_error());
	mysqli_select_db($db_server, $db_database) or die("Unable to select database: " . mysqli_error());
	$db_server->set_charset("utf8"); //Lo usamos para que la conexión a la BD use utf8

	/*
	* Obtenemos el título de la película que se proyecta durante la fecha seleccionada
	*/
	$query = "SELECT titolPeli from SESSIO where (diaSessio = '$fechaSessio')";
	$result = mysqli_query($db_server, $query);
	$titol = mysqli_fetch_row($result);
	$titol = $titol[0];

	
	/*
	* Debemos identificar si la sesión será normal o especial (Dia espectador) para poder usar
	* los precios correspondientes
	*/
	$query = "SELECT sessioEspecial from SESSIO where (diaSessio = '$fechaSessio')";
	$result = mysqli_query($db_server, $query);
	$rows = mysqli_num_rows($result);
	
	//En caso que se devuelva un registro vacío, debemos indicarlo como error
	if (!$rows) {
		header('Location: ../Errores/errorAdminConsultaSessio.php');
		exit;
	}
	//En caso de éxito, nos guardamos el valor devuelto
	$esSessioEspecial = mysqli_fetch_row($result);
	$esSessioEspecial = $esSessioEspecial[0];


	/*
	* Debemos identificar si la sesión asociada a la fecha seleccionada
	* contiene butacas VIP o no
	*/
	$query = "SELECT sessioVip from SESSIO where (diaSessio = '$fechaSessio')";
	$result = mysqli_query($db_server, $query);
	$rows = mysqli_num_rows($result);
	
	//En caso que se devuelva un registro vacío, debemos indicarlo como error
	if (!$rows) {
		header('Location: ../Errores/errorAdminConsultaSessio.php');
		exit;
	}
	//En caso de éxito, nos guardamos el valor devuelto
	$esSessioVip = mysqli_fetch_row($result);
	$esSessioVip = $esSessioVip[0];

	
	/*
	* Debemos obtener las butacas, en caso que las tenga, asociadas ya a esa sesión,
	* es decir, las butacas que ya estan ocupadas en esa sala.
	*/
	$subquery = "SELECT idSessio from SESSIO where (diaSessio = '$fechaSessio')";
	$query = "SELECT numButaca from ENTRADA where (idSessio = ($subquery))";
	$result = mysqli_query($db_server, $query);
	$rows = mysqli_num_rows($result);
	
	//Controlamos si el patio de butacas esta vacío o ya tiene alguna de estas asignada
	$patioButacasVacio = false;
	if (!$rows) {
		$patioButacasVacio = true;
	}
	
	//Guardamos los registros devueltos en forma de vector para poder acceder mejor a ellos después
	$butacasOcupadasBD = array();
	for ($i = 0; $i < $rows; $i++) {
		$consulta = mysqli_fetch_assoc($result);
	
		foreach ($consulta as $key => $valor) {
			array_push($butacasOcupadasBD, $valor);
		}
	}


	//Nos creamos unas variables para controlar, assignar y posteriormente, si se requiere, modificarlas facilmente tanto el precio
	//como la correspondiente fila VIP
	$preuButacaNoVip = 6;
	$preuButacaVip = 8;
	$preuButacaNoVipEspepcial = 4;
	$preuButacaVipEspepcial = 6;
	$filaVip = 6;

	/*
	* Generaremos el patio de butacas, 12 filas por 10 columnas, mediante los datos obtenidos anteriormente
	*/
	$patioButacas = '<table class="butacasAdmin">';

	for ($i = 1; $i <= 12; $i++) {

		/*
		* Como indica el enunciado, las filas van de A-L, así que las debemos convertir
		*/
		switch($i) {
			case 1:
				$letraFila = 'A';
				break;
			case 2:
				$letraFila = 'B';
				break;
			case 3:
				$letraFila = 'C';
				break;
			case 4:
				$letraFila = 'D';
				break;
			case 5:
				$letraFila = 'E';
				break;
			case 6:
				$letraFila = 'F';
				break;
			case 7:
				$letraFila = 'G';
				break;
			case 8:
				$letraFila = 'H';
				break;
			case 9:
				$letraFila = 'I';
				break;
			case 10:
				$letraFila = 'J';
				break;
			case 11:
				$letraFila = 'K';
				break;
			case 12:
				$letraFila = 'L';
				break;
		}

		/*
		* Ahora, rellenaremos el patio de butacas con la correspondiente imagen de la butaca, segun sea ocupada, seleccionada o libre
		*/
		$patioButacas .= '<tr>';
		for ($j = 1; $j <= 10; $j++) {
			//Guardamos el codigo/numero de butaca que usaremos para identificarla
			$numButuacaActual = $letraFila . $j;
			$butacaOcupadaEncontrada = false;

			//Primero buscamos las butacas ocupadas y las tratamos con su imagen correspondiente
			for($k = 0; $k < $rows && !$butacaOcupadaEncontrada; $k++) {
				if (!$patioButacasVacio && $numButuacaActual == $butacasOcupadasBD[$k]) {
					//Si hay alguna butaca en el vector que devuelve la consulta, pintamos como ocupada
					$patioButacas .= '<td class="ocupat" numButaca="' . $numButuacaActual . '"></td>';
					$butacaOcupadaEncontrada = true;
				}
			}

			//En caso de que no encontremos ninguna butaca ocupada, miramos de que tipo de las que quedan es
			if (!$butacaOcupadaEncontrada) {
				//Si la sesion es una normal, los precios seran de 6 y 8 € (normales, vips)
				if (!$esSessioEspecial) {
					if (($i == 1 && ($j == 1 || $j == 2))) {
						//Butacas para personas con dificultades fisicas
						$patioButacas .= '<td class="minu" numButaca="' . $numButuacaActual . '" preu = "'. $preuButacaNoVip . '"></td>';
					}
					else if ($i == $filaVip && $esSessioVip) {
						//Gestion butacas vip
						$patioButacas .= '<td class="lliure VIP" numButaca="' . $numButuacaActual . '" preu = "'. $preuButacaVip . '"></td>';
					}
					else {
						//Butacas libres
						$patioButacas .= '<td class="lliure" numButaca="' . $numButuacaActual . '"  preu = "'. $preuButacaNoVip . '"></td>';
					}
				}
				//En caso contrario, se trata de una sesion del dia del espectador, los precios seran de 4 y 6 €
				else {
					if (($i == 1 && ($j == 1 || $j == 2))) {
						//Butacas para personas con dificultades fisicas
						$patioButacas .= '<td class="minu" numButaca="' . $numButuacaActual . '" preu = "'. $preuButacaNoVipEspepcial . '"></td>';
					}
					else if ($i == $filaVip && $esSessioVip) {
						//Gestion PROVISIONAL butacas vip
						$patioButacas .= '<td class="lliure VIP" numButaca="' . $numButuacaActual . '" preu = "'. $preuButacaVipEspepcial . '"></td>';
					}
					else {
						//Butacas libres
						$patioButacas .= '<td class="lliure" numButaca="' . $numButuacaActual . '"  preu = "'. $preuButacaNoVipEspepcial . '"></td>';
					}
				}
				
			}
		}
		$patioButacas .= '</tr>';
	}
	$patioButacas .= '</table>';

	/**** GENERAMOS GRAFICOS QUESITOS *****/
	/*
	* Obtenemos los datos necesarios para poder crear los graficos
	* Obtener el numero de entradas de esa sesion, el total de butacas que tiene la sala
	* y las entradas diferenciadas por si son vip o no
	*/
	$subquery = "SELECT idSessio from SESSIO where (diaSessio = '$fechaSessio')";
	$query = "SELECT count(*) from ENTRADA where (idSessio = ($subquery))";
	$result = mysqli_query($db_server, $query);
	$rows = mysqli_num_rows($result);
	
	if (!$rows) {
		header('Location: ../Errores/errorAdminConsultaEntradesSessio.php');
		exit;
    }
	
	$entradasVendidas = 0;
	
	for ($i = 0; $i < $rows; $i++) {
		$consulta = mysqli_fetch_assoc($result);
		foreach ($consulta as $key => $valor) {
            $entradasVendidas = $valor;
		}
	}

	$querySala = "SELECT salaSessio from SESSIO where (idSessio = ($subquery))";
	$query = "SELECT totalButacas from SALA where (numSala = ($querySala))";
	$result = mysqli_query($db_server, $query);
	$rows = mysqli_num_rows($result);

	if (!$rows) {
		header('Location: ../Errores/errorAdminConsultaButaquesSala.php');
		exit;
	}
	
	$totalButacasSala = 0;
	
	for ($i = 0; $i < $rows; $i++) {
		$consulta = mysqli_fetch_assoc($result);
		foreach ($consulta as $key => $valor) {
            $totalButacasSala = $valor;
		}
	}
	$entradasNoVendidas = $totalButacasSala - $entradasVendidas;


	/**** INFORMACION SOBRE LAS ENTRADAS POR TIPOS ****/
	/**
	 * Como he indicado antes, diferenciamos entre las entradas vip y las normales para informar al admministrador
	*/
	if ($esSessioEspecial) {
		if ($esSessioVip) {
			$queryVip = "SELECT count(*) from ENTRADA where preu = $preuButacaVipEspepcial and idSessio = ($subquery)";
		}
		$query = "SELECT count(*) from ENTRADA where preu = $preuButacaNoVipEspepcial and idSessio = ($subquery)";
	}
	else {
		if ($esSessioVip) {
			$queryVip = "SELECT count(*) from ENTRADA where preu = $preuButacaVip and idSessio = ($subquery)";
		}
		$query = "SELECT count(*) from ENTRADA where preu = $preuButacaNoVip and idSessio = ($subquery)";
	}

	//Cantidad de entradas no vips vendias en esa sesion
	$result = mysqli_query($db_server, $query);
	$rows = mysqli_num_rows($result);
	
	if (!$rows) {
		header('Location: ../Errores/errorAdminConsultaEntradesNoVip.php');
		exit;
	}
	$cantidadEntradasNoVipVendidas = mysqli_fetch_row($result);
	$cantidadEntradasNoVipVendidas = $cantidadEntradasNoVipVendidas[0];

	$cantidadEntradasVipVendidas = 0;
	if ($esSessioVip) {
		//Cantidad de entradas vips vendidas en esa sesion
		$result = mysqli_query($db_server, $queryVip);
		$rows = mysqli_num_rows($result);
		
		if (!$rows) {
			header('Location: ../Errores/errorAdminConsultaEntradesVip.php');
			exit;
		}
		$cantidadEntradasVipVendidas = mysqli_fetch_row($result);
		$cantidadEntradasVipVendidas = $cantidadEntradasVipVendidas[0];
	}

	//Generamos el html a mostrar con la informacion
	
	//Si no se han vendido entradas mostraremos otro mensaje
	$mostrarInfoTipoEntradas = true;
	if (!$cantidadEntradasNoVipVendidas && !$cantidadEntradasVipVendidas) {
		$mostrarInfoTipoEntradas = false;
		$mensajeNoEntradasVendidas = "<p>No s'ha venut cap entrada en aquesta sessió.</p>";
	}
	else {
		//Primero calculamos el recaudo
		if ($esSessioEspecial) {
			$recaudoEntradasVip = $cantidadEntradasVipVendidas * $preuButacaVipEspepcial;
			$recaudoEntradasNoVip = $cantidadEntradasNoVipVendidas * $preuButacaNoVipEspepcial;
		}
		else {
			$recaudoEntradasVip = $cantidadEntradasVipVendidas * $preuButacaVip;
			$recaudoEntradasNoVip = $cantidadEntradasNoVipVendidas * $preuButacaNoVip;
		}

		$infoTiposEntradas = "<p>";
		$infoTiposEntradas .= "S'han venut <strong>" . $cantidadEntradasVipVendidas . "</strong> entrades <strong>VIP</strong> durant aquesta sessió. El recaudat per aquestes entrades és un total de <strong>" . $recaudoEntradasVip ."€</strong>.";
		$infoTiposEntradas .= "</p>";
		$infoTiposEntradas .= "<p>";
		$infoTiposEntradas .= "S'han venut <strong>" . $cantidadEntradasNoVipVendidas . "</strong> entrades <strong>no VIP</strong> durant aquesta sessió. El recaudat per aquestes entrades és un total de <strong>" . $recaudoEntradasNoVip ."€</strong>.";
		$infoTiposEntradas .= "</p>";
	}
	
	/**** INFORMACION TOTAL RECAUDADO POR TODAS LAS ENTRADAS ****/
	/**
	 * Informamos de la recaudacion total que lleva la sala
	 */
	$recaudoTotalSesion = $recaudoEntradasNoVip + $recaudoEntradasVip;
	$infoTotalEntradas = "<p>Aquesta sessió ha recaudat un total de <strong>" . $recaudoTotalSesion . "€</strong>.</p>";


	mysqli_close($db_server);
?>


<!DOCTYPE html>

<html lang='ca'>

<head>
	<meta charset="UTF-8">
	<title>I AM CINEMA</title>
	<link rel="stylesheet" href="../../css/reset.css">
	<link rel="stylesheet" href="../../css/normalize.css">
  	<link rel="stylesheet" href="../../css/skeleton.css">
	<link rel=stylesheet href="../../css/style.css">
	<link rel=stylesheet href="../../css/styleButaques.css">
	<link rel=stylesheet href="../../css/graficos.css">
	<script src="../../js/jquery-3.3.1.min.js"></script>
	<script src="../../js/loader.js"></script>
	
	<script>
		google.charts.load("current", { packages: ["corechart"] });
		google.charts.setOnLoadCallback(drawChart);	

		function drawChart() {
			let data = google.visualization.arrayToDataTable([
				['Estat', 'Ocupació'],
				['Entrades Venudes', <?php echo $entradasVendidas ?>],
				['Entrades Lliures', <?php echo $entradasNoVendidas ?>]
			]);

			let options = {
				title: 'Ocupació <?php echo $fechaSessio; ?>',
				//Llegenda
				legend: 'yes',
				//Percentatge en la gràfica
				pieSliceText: 'yes',
				//3D
				is3D: true,
				//Colors
				slices: {
					0: { color: '#FF9B21' },
					1: { color: '#63639c', offset: 0.3 },
				}
			};

			let chart = new google.visualization.PieChart(document.getElementById('piechart'));
			chart.draw(data, options);
		}

	</script>

	<link rel="icon" type="image/png" href="../../img/icon.png">
</head>

<body>
	<?php include("../Includes/header.php"); ?>

	<div class="content">
		<h1>Informació de la Sessió</h1>

		<h2>
			<?php echo $titol ?>
		</h2>

		<div class="container">
			<div class="row">
				<div class="twelve columns" id="content">
					<div class="butacas">
						<?php echo $patioButacas ?>
					</div>
					<div class="info">
						<div id="piechart" style="width: 600px; height: 200px;"></div>
					
						<?php 
							if ($esSessioEspecial) {
								echo "<p>Aquesta sessió és del dia de l'espectador.</p>";
							}
							if (!$mostrarInfoTipoEntradas) {
								echo $mensajeNoEntradasVendidas;
							}
							else {
								echo $infoTiposEntradas;
								echo $infoTotalEntradas;
							}
						?>
						<a class="button" href="./seleccioSessio.php">ANTERIOR</a>

						<a class="button botonFooterSEG" href="../../index.php">INICI</a>
					</div>

				</div>
			</div>
		</div>
	</div>

	<?php include("../Includes/footer.php"); ?>
</body>

</html>