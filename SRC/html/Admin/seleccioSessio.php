<!DOCTYPE html>

<html lang='ca'>

<head>
	<meta charset="UTF-8">
	<title>I AM CINEMA</title>
	<link rel="stylesheet" href="../../css/reset.css">
	<link rel="stylesheet" href="../../css/normalize.css">
	<link rel="stylesheet" href="../../css/skeleton.css">	
	<link rel=stylesheet href="../../css/style.css">
	<link rel=stylesheet href="../../css/styleCalendario.css">
	<script src="../../js/jquery-3.3.1.min.js"></script>
	<script src="../../js/funcionesCalendarioAdmin.js"></script>
	<link rel="icon" type="image/png" href="../../img/icon.png">
</head>

<body>
	<?php include("../Includes/header.php"); ?>

	<div class="content">
		
		<h1>Selecció de sessió</h1>

		<div class="row">
			<div class="three columns centro">
				<button id="retroceder" type="button" class="">Anterior</button>
			</div>
			<form action='infoSessio.php' method='post'>
				<div id="calendario" class="six columns"></div>
			</form>
			
			<div class="three columns centro">
				<button id="avanzar" type="button" class="">Següent</button>
			</div> 
		</div>

	</div>

	<?php include("../Includes/footer.php"); ?>
</body>

</html>