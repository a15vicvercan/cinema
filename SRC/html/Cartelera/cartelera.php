<?php
	require_once '../../php/login.php';
	$db_server = mysqli_connect($db_hostname, $db_username, $db_password, $db_database);
	if (!$db_server) die("Unable to connect to MySQL: " . mysql_error());
	mysqli_select_db($db_server, $db_database) or die("Unable to select database: " . mysqli_error());
	$db_server->set_charset("utf8"); //Lo usamos para que la conexión a la BD use utf8

	/**
	 * Obtenemos los carteles de todas las peliculas de la BD
	 */
	$query = "SELECT `cartell` FROM `PELICULA`";
	$result = mysqli_query($db_server, $query);
	$rows = mysqli_num_rows($result);
	//Guardo las imagenes de las peliculas en un vector
	$cartelesPelis = array();
	for ($i = 0; $i < $rows; $i++) {
		$consulta = mysqli_fetch_assoc($result);
		foreach ($consulta as $key => $valor) {
			array_push($cartelesPelis, $valor);
		}
	}

	/**
	 * Obtenemos todos los titulos de las peliculas de la BD
	 */
	$query = "SELECT `titol` FROM `PELICULA` ";
	$titol = mysqli_query($db_server, $query);
	//Guardo los titulos de las peliculas en un vector
	$titolPelis = array();
	for ($i = 0; $i < $rows; $i++) {
		$consulta1 = mysqli_fetch_assoc($titol);
		foreach ($consulta1 as $key => $valor) {
			array_push($titolPelis, $valor);
		}
	}

	/**** GENERAMOS LA CARTELERA ****/
	/**
	 * Geramos una especie de tabla de 3 columnas
	 */
	$iteradorPelicula = 0;
	$cartelera .= "<table class = 'cartelera'>";
	for($i = 0; $i < $rows/3; $i++){
		$cartelera .= "<tr>";
		for($j = 0; $j < 3; $j++){
			if($cartelesPelis[$iteradorPelicula] != ""){
				$cartelera .= "<td class='celdaCartelera' titol='$titolPelis[$iteradorPelicula]'><button class='peliculaCartelera' type='submit'><img class='CartellPeli' src='$cartelesPelis[$iteradorPelicula]' alt='Cartellera'></button></td>";
				$iteradorPelicula++;
			}
			else {
				$cartelera .= "<td class='celdaCartelera'></td>";
			}
		}
		if($i%3==0){
			$cartelera .= "</tr>";
		}
	}
	$cartelera .= "</table>";
	mysqli_close($db_server);
?>

<!DOCTYPE html>

<html lang="ca">

<head>
	<meta charset="UTF-8">
	<title>I AM CINEMA</title>
	<link rel="stylesheet" href="../../css/reset.css">
	<link rel="stylesheet" href="../../css/normalize.css">
  	<link rel="stylesheet" href="../../css/skeleton.css">
	<link rel=stylesheet href="../../css/style.css">
	<link rel=stylesheet href="../../css/styleCartelera.css">
	<script src="../../js/jquery-3.3.1.min.js"></script>
	<script src="../../js/funcionesCartelera.js"></script>
	<link rel="icon" type="image/png" href="../../img/icon.png">
</head>

<body>
	<?php include("../Includes/header.php"); ?>

	<div class="content">
		
		<h1>Cartellera</h1>

		<form action='../Usuari/infoSessioSeleccionada.php' class="formulariPeliculaCartelera" method='post'>
			<?php			  
				echo $cartelera;
			?>
		</form>

		<a class="button botonFooter" href="../../index.php">ANTERIOR</a>

	</div>

	<?php include("../Includes/footer.php"); ?>
</body>

</html>