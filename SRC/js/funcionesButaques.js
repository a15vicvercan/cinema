//Generamos el campo oculto para pasar el precio y el num de butacas
function generarCampoOculto(butacasSeleccionadas) {
  let campoOcultoPatioButacas = "<input type='hidden' name='butacasSeleccionadas' value='" + butacasSeleccionadas + "'/>";

  return campoOcultoPatioButacas;
}

$(document).ready(function () {
  let maxButSel = 0;
  let butacas = ["<pre>Historial<br>"];

  let preuTotal = 0;

  //$("#content").html(sala());

  $("td").hover(
    function () {
      if (!$(this).hasClass("ocupat")) {
        $(this).addClass("marca");
      }
    }, function () {
      $(this).removeClass("marca");
    }
  );

  $("td").click(function () {
    let but = $(this).attr("class");

    let preuButaca = parseInt($(this).attr("preu"));

    if ($(this).attr("class") == 'ocupat') {
      Swal({
        type: 'error',
        title: 'Oops...',
        text: 'No pots seleccionar aquesta butaca, està ocupada'
      })
    }

    else if (maxButSel == 10 && ($(this).hasClass("minu") || $(this).hasClass("lliure"))) {
      Swal({
        type: 'error',
        title: 'Oops...',
        text: 'No pots seleccionar més de 10 butaques'
      })
    }

    else {
      let butGuard = '<table class="table">';
      let col = ($(this).index()) + 1;
      let fil = ($(this).closest('tr').index()) + 1;

      if (fil == 1) fil = "A";
      else if (fil == 2) fil = "B";
      else if (fil == 3) fil = "C";
      else if (fil == 4) fil = "D";
      else if (fil == 5) fil = "E";
      else if (fil == 6) fil = "F";
      else if (fil == 7) fil = "G";
      else if (fil == 8) fil = "H";
      else if (fil == 9) fil = "I";
      else if (fil == 10) fil = "J";
      else if (fil == 11) fil = "K";
      else fil = "L";

      if ($(this).hasClass("minu")) {
        $($(this)).fadeOut(50, function () {
          $($(this)).removeClass("minu");
          $($(this)).addClass("sel2");
          $(this).fadeIn(50);
        });
        butGuard += '<tr><img src="../../img/butaques/min2Small.png" alt="Butacas minu"> Fila: ' + fil + ' - Butaca: ' + col + '</tr>';
        butacas.push(butGuard);
        maxButSel++;
          
        preuTotal += preuButaca;
      }

      else if ($(this).hasClass("lliure")) {
        $($(this)).fadeOut(50, function () {
          $($(this)).removeClass("lliure");
          $($(this)).addClass("sel");
          $(this).fadeIn(50);
        });
        if ($(this).hasClass("VIP")) {

          butGuard += '<tr><img src="../../img/butaques/bmVIPSmall.png" alt="Butacas normal"> Fila: ' + fil + ' - Butaca: ' + col + '</tr>';
          preuTotal += preuButaca;
        }
        else {
          butGuard += '<tr><img src="../../img/butaques/bm2Small.png" alt="Butacas normal"> Fila: ' + fil + ' - Butaca: ' + col + '</tr>';
          preuTotal += preuButaca;
        }
        butacas.push(butGuard);
        maxButSel++;
      }

      else if ($(this).hasClass("sel")) {
        $($(this)).fadeOut(50, function () {
          $($(this)).removeClass("sel");
          $($(this)).addClass("lliure");
          $(this).fadeIn(50);
        });
        if ($(this).hasClass("VIP")) {
          butGuard += '<tr><img src="../../img/butaques/bmVIPSmall.png" alt="Butacas normal"> Fila: ' + fil + ' - Butaca: ' + col + '</tr>';
          preuTotal -= preuButaca;
        }
        else {
          butGuard += '<tr><img src="../../img/butaques/bm2Small.png" alt="Butacas normal"> Fila: ' + fil + ' - Butaca: ' + col + '</tr>';
          preuTotal -= preuButaca;

        }
        let index = butacas.indexOf(butGuard);
        if (index > -1) {
          butacas.splice(index, 1);
        }
        maxButSel--;
      }

      else { //sel2
        $($(this)).fadeOut(50, function () {
          $($(this)).removeClass("sel2");
          $($(this)).addClass("minu");
          $(this).fadeIn(50);
        });
        butGuard += '<tr><img src="../../img/butaques/min2Small.png" alt="Butacas minu"> Fila: ' + fil + ' - Butaca: ' + col + '</tr>';
        let index = butacas.indexOf(butGuard);
        if (index > -1) {
          butacas.splice(index, 1);
        }
        maxButSel--;
        preuTotal -= preuButaca;
      }
      butGuard += '</table>';
    }
    $("#preu").html(preuTotal + '€');
    $("#panel").html(butacas);
  });

  $(".formulariFinalCompra").submit(function (evt) {
    evt.preventDefault();

    let ButacSelect = "";
    $(".sel2").each(function () {
      let numButaca = $(this).attr("numButaca");
      let preuButaca = $(this).attr("preu");
      ButacSelect += numButaca + '#' + preuButaca + '#';
    });
    $(".sel").each(function () {
      let numButaca = $(this).attr("numButaca");
      let preuButaca = $(this).attr("preu");
      ButacSelect += numButaca + '#' + preuButaca + '#';
    });
    let campoOcultoButacas = generarCampoOculto(ButacSelect);
    $("form").append(campoOcultoButacas);

    evt.currentTarget.submit();
  });
});