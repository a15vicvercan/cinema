function generarCampoOculto(PeliSelect) {

    let campoOcultoCartelera = "<input type='hidden' name='peliculaSeleccionada' value='" + PeliSelect + "'/>";
  
    return campoOcultoCartelera;
}

$(document).ready(function () {
    
    $(".formulariPeliculaCartelera").submit(function (evt) {
        evt.preventDefault();

        let PeliSelect = "";
        $("td").click(function () {
            PeliSelect = $(this).attr("titol");

            let campoOcultoCartelera = generarCampoOculto(PeliSelect);
            $("form").append(campoOcultoCartelera);

            evt.currentTarget.submit();
        });
        
    });
});