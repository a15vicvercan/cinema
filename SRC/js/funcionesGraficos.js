google.charts.load("current", { packages: ["corechart"] });
google.charts.setOnLoadCallback(drawChart);
//Amb VIP
/*function drawChart() {

    let data = google.visualization.arrayToDataTable([
        ['Estat', 'Ocupació'],
        ['Normals Venudes', 91],
        ['VIP Venudes', 6],
        ['VIP Lliures', 4],
        ['Normals Lliures', 19]
    ]);

    let options = {
        title: 'Butaques 27/12/2018',
        //Llegenda
        legend: 'yes',
        //Percentatge en la gràfica
        pieSliceText: 'yes',
        //3D
        is3D: true,
        //Colors
        slices: {
            0: { color: '#EF2929' },
            1: { color: '#EDD400' },
            2: { color: '#C4A000', offset: 0.3 }, //Separació
            3: { color: '#DC3912', offset: 0.3 },
        }
    };

    let chart = new google.visualization.PieChart(document.getElementById('piechart'));
    chart.draw(data, options);
}*/
//SENSE VIP
function drawChart() {

    let data = google.visualization.arrayToDataTable([
        ['Estat', 'Ocupació'],
        ['Normals Venudes', 91],
        ['Normals Lliures', 19]
    ]);

    let options = {
        title: 'Butaques 27/12/2018',
        //Llegenda
        legend: 'yes',
        //Percentatge en la gràfica
        pieSliceText: 'yes',
        //3D
        is3D: true,
        //Colors
        slices: {
            0: { color: '#63639c' },
            1: { color: '#FF9B21', offset: 0.3 },
        }
    };

    let chart = new google.visualization.PieChart(document.getElementById('GraficoQueso'));
    chart.draw(data, options);
}


/*google.charts.load('current', {'packages':['corechart']});
    google.charts.setOnLoadCallback(drawChart);

    function drawChart() {    

    var data = google.visualization.arrayToDataTable([
        ['Estat', 'Ocupacio'],
        ['Ocupadas',     10],
        ['Vacias',      90]
    ]);

    var options = {
        title: 'Ocupacio de la sessió.'
    };

    var chart = new google.visualization.PieChart(document.getElementById('piechart'));

    chart.draw(data, options);
}*/

/*google.charts.load("current", {packages:["imagebarchart"]});
      google.charts.setOnLoadCallback(drawChart);

      function drawChart() {

        var data = google.visualization.arrayToDataTable([
          ['Year', 'Sales', 'Expenses'],
          ['2004',  1000,      400],
          ['2005',  1170,      460],
          ['2006',  660,       1120],
          ['2007',  1030,      540]
        ]);

        var chart = new google.visualization.ImageBarChart(document.getElementById('chart_div'));

        chart.draw(data, {width: 400, height: 240, min: 0});
      }
*/