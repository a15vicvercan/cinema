let moduloCalendario = (function () {
    function diainici(mes, any) {
        let inici = new Date(any, mes - 1, 1).getDay();
        if (inici == 0) inici = 6;
        else inici--;

        return inici;
    }

    function diasTotal(mes, any) {
        return new Date(any, mes, 0).getDate();
    }

    function nom(mes, any) {

        if (mes == 1) titol = "<h3 class='px-5'>Gener de " + any + "</h3>";
        else if (mes == 2) titol = "<h3 class='px-5'>Febrer de " + any + "</h3>";
        else if (mes == 3) titol = "<h3 class='px-5'>Març de " + any + "</h3>";
        else if (mes == 4) titol = "<h3 class='px-5'>Abril de " + any + "</h3>";
        else if (mes == 5) titol = "<h3 class='px-5'>Maig de " + any + "</h3>";
        else if (mes == 6) titol = "<h3 class='px-5'>Juny de " + any + "</h3>";
        else if (mes == 7) titol = "<h3 class='px-5'>Juliol de " + any + "</h3>";
        else if (mes == 8) titol = "<h3 class='px-5'>Agost de " + any + "</h3>";
        else if (mes == 9) titol = "<h3 class='px-5'>Setembre de " + any + "</h3>";
        else if (mes == 10) titol = "<h3 class='px-5'>Octubre de " + any + "</h3>";
        else if (mes == 11) titol = "<h3 class='px-5'>Novembre de " + any + "</h3>";
        else titol = "<h3 class='px-5'>Desembre de " + any + "</h3>";

        return titol;
    }

    function creaCalendari(dia, mes, any) {

        let data = diainici(mes, any);

        var hoy = new Date();
        let diaActual = hoy.getDate();
        let mesActual = hoy.getMonth() + 1;
        let anyActual = hoy.getFullYear();

        let diasMes = diasTotal(mes, any);

        let dies_mes_ant = new Date(any, mes - 1, 0).getDate();
        let mes_ant = 0;
        let dia_seg = 1;

        let cont = 1;

        let calendari = "";
        calendari += "<table class=''>";
        calendari += "<tr><th class='centro'>Dl</th><th class='centro'>Dm</th><th class='centro'>Dc</th><th class='centro'>Dj</th><th class='centro'>Dv</th><th class='centro'>Ds</th><th class='centro'>Dg</th></tr>";

        let diaAnt = data - 1;

        calendari += "<tbody>";

        for (let i = 1; i <= 6; i++) {
            calendari += "<tr>";
            for (let j = 1; j <= 7; j++) {
                if(mes == mesActual && any == anyActual){
                    if (data > 0) {
                        calendari += "<td><p class='anterior'>";
                        mes_ant = dies_mes_ant - diaAnt;
                        calendari += mes_ant;
                        calendari += "</p></td>";
                        diaAnt--;
                        data--;
                    }
                    else {
                        if (dia == cont && mes == mesActual && any == anyActual) {
                            calendari += "<td class='diaActual " + cont + "'><button class='dataCalendari' type='submit'>";
                            calendari += cont;
                            calendari += "</button></td>";
                            cont++;
                        }
                        else if (cont > dia && cont <= diasMes) {
                            //lleno con dias
                            calendari += "<td class='" + cont + "'><button class='dataCalendari' type='submit'>" + cont + "</button></td>";
                            cont++;
                        }
                        else if (cont < dia) {
                            //lleno con dias
                            calendari += "<td class='" + cont + "'><p>" + cont + "</p></td>";
                            cont++;
                        }
                        else {
                            calendari += "<td><p class='anterior'>";
                            calendari += dia_seg;
                            calendari += "</p></td>";
                            dia_seg++;
                        }
                    }
                }
                else if((mes > mesActual && any == anyActual) || any > anyActual){
                    if (data > 0) {
                        calendari += "<td><p class='anterior'>";
                        mes_ant = dies_mes_ant - diaAnt;
                        calendari += mes_ant;
                        calendari += "</p></td>";
                        diaAnt--;
                        data--;
                    }
                    else {
                        if (cont <= diasMes) {
                            //lleno con dias
                            calendari += "<td class='" + cont + "'><button class='dataCalendari' type='submit'>" + cont + "</button></td>";
                            cont++;
                        }
                        else {
                            calendari += "<td><p class='anterior'>";
                            calendari += dia_seg;
                            calendari += "</p></td>";
                            dia_seg++;
                        }
                    }
                }
                else {
                    if (data > 0) {
                        calendari += "<td><p class='anterior'>";
                        mes_ant = dies_mes_ant - diaAnt;
                        calendari += mes_ant;
                        calendari += "</p></td>";
                        diaAnt--;
                        data--;
                    }
                    else {
                        if (cont <= diasMes) {
                            //lleno con dias
                            calendari += "<td class='" + cont + "'><p>" + cont + "</p></td>";
                            cont++;
                        }
                        else {
                            calendari += "<td><p class='anterior'>";
                            calendari += dia_seg;
                            calendari += "</p></td>";
                            dia_seg++;
                        }
                    }
                }
                
            }
            calendari += "</tr>";
        }

        calendari += "</tbody>";

        calendari += "</table>";

        return calendari;
    }

    function generarCampoOculto(dia, mes, any) {
        let formulari = "<input type='hidden' name='data' value='" + dia + "#" + mes + "#" + any + "'/>";

        return formulari;
    }

    return {
        nomMes: nom,
        generarCalendari: creaCalendari,
        pasarData: generarCampoOculto
    };

})();

$(document).ready(function () {
    var hoy = new Date();
    let dia = hoy.getDate();
    let mes = hoy.getMonth() + 1;
    let any = hoy.getFullYear();

    let tabla = moduloCalendario.generarCalendari(dia, mes, any);
    let nombreMes = moduloCalendario.nomMes(mes, any);

    $("#calendario").html(nombreMes);
    $("#calendario").append(tabla);

    $("#retroceder").click(function() {
        if (mes == 1) {
            mes = 12;
            any--;
        }
        else mes--;

        let tabla = moduloCalendario.generarCalendari(dia, mes, any);
        let nombreMes = moduloCalendario.nomMes(mes, any);
        $("#calendario").html(nombreMes);
        $("#calendario").append(tabla);

        $("td").click(function() {
            let diaClickat = $(this).attr("class");
            let fecha = moduloCalendario.pasarData(diaClickat, mes, any);
            $("form").append(fecha);
        })
    })

    $("#avanzar").click(function() {
        if (mes == 12) {
            mes = 1;
            any++;
        }
        else mes++;

        let tabla = moduloCalendario.generarCalendari(dia, mes, any);
        let nombreMes = moduloCalendario.nomMes(mes, any);
        $("#calendario").html(nombreMes);
        $("#calendario").append(tabla);

        $("td").click(function() {
            let diaClickat = $(this).attr("class");
            let fecha = moduloCalendario.pasarData(diaClickat, mes, any);
            $("form").append(fecha);
        })
    })

    $("td").click(function() {
        let diaClickat = $(this).attr("class");
        let fecha = moduloCalendario.pasarData(diaClickat, mes, any);
        $("form").append(fecha);
    })

})