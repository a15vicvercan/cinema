let slideIndex = 1;

function plusSlides(n) {
  showSlides(slideIndex += n);
}

function minuSlides(n) {
    showSlides(slideIndex -= n);
}

function currentSlide(n) {
  showSlides(slideIndex = n);
}

function showSlides(n) {
    let i;
    let slides = document.getElementsByClassName("mySlides");
    if (n > slides.length) {
        slideIndex = 1
    }    
    if (n < 1) {
        slideIndex = slides.length
    }
    for (i = 0; i < slides.length; i++) {
        slides[i].style.display = "block";  
    }
  
    slides[slideIndex-1].style.display = "none";  
}

function generarCampoOculto(PeliSelect) {

    let campoOcultoCartelera = "<input type='hidden' name='peliculaSeleccionada' value='" + PeliSelect + "'/>";
  
    return campoOcultoCartelera;
}

$(document).ready(function () {
    
    $(".formulariPeliculaCartelera").submit(function (evt) {
        evt.preventDefault();

        let PeliSelect = "";
        $("button").click(function () {
            PeliSelect = $(this).attr("titol");
            console.log(PeliSelect);
            let campoOcultoCartelera = generarCampoOculto(PeliSelect);
            $("form").append(campoOcultoCartelera);

            evt.currentTarget.submit();
        });
        
    });
});