$(document).ready(function() {
    
    $("#mail").keyup(function() {
        let valor = $(this).val();
        console.log(valor);
        let valid = valor.match(/^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.([a-zA-Z]{2,4})+$/);
        console.log(valid);
        if (!valid) {
            $("#mail").addClass("error");
        }
        else {
            $("#mail").removeClass("error");
        }
    })
})