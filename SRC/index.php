<?php
	require_once 'php/login.php';
	$db_server = mysqli_connect($db_hostname, $db_username, $db_password, $db_database);
	if (!$db_server) die("Unable to connect to MySQL: " . mysql_error());
	mysqli_select_db($db_server, $db_database) or die("Unable to select database: " . mysqli_error());
	$db_server->set_charset("utf8"); //Lo usamos para que la conexión a la BD use utf8

	//Obtenemos los carteles de las peliculas para ponerlo en el carrusel
	$query = "SELECT `cartell` FROM `PELICULA` LIMIT 10";
	$result = mysqli_query($db_server, $query);
	$rows = mysqli_num_rows($result);

	//Guardo las imagenes de las peliculas en un vector
	$cartelesPelis = array();
	for ($i = 0; $i < $rows; $i++) {
		$consulta = mysqli_fetch_assoc($result);
		foreach ($consulta as $key => $valor) {
			array_push($cartelesPelis, $valor);
		}
	}

	//Guardamos los titulos de las peliculas
	$query = "SELECT `titol` FROM `PELICULA` ";
	$titol = mysqli_query($db_server, $query);

	//Guardo los titulos de las peliculas en un vector
	$titolPelis = array();
	for ($i = 0; $i < $rows; $i++) {
		$consulta1 = mysqli_fetch_assoc($titol);
		foreach ($consulta1 as $key => $valor) {
			array_push($titolPelis, $valor);
		}
	}

	//Adaptamos la ruta de la imagen
	for($i = 0; $i < $rows; $i++){
		$cartelesPelis[$i] = str_replace("../", "", $cartelesPelis[$i]);
	}

	/**** CREAMOS EL CARRUSEL ****/

	$iteradorPelicula = 0;

	for($i = 0; $i < $rows/5; $i++){
		if($i == 1) {
			$carrusel .= "<div class='mySlides fade' style='display: block'>";
		}
		else {
			$carrusel .= "<div class='mySlides fade' style='display:none'>";
		}
		for($j = 0; $j < 5; $j++){
			if($cartelesPelis[$iteradorPelicula] != ""){
				$carrusel .= "<button titol='$titolPelis[$iteradorPelicula]' type='submit'><img class='CartellPeli' src='$cartelesPelis[$iteradorPelicula]' alt='Imatge'></button>";
				$iteradorPelicula++;
			}
			else {
				$carrusel .= "";
			}
		}
		$carrusel .= "</div>";
	}
	mysqli_close($db_server);
?>



<!DOCTYPE html>

<html lang="ca">

<head>
	<meta charset="UTF-8">
	<title>I AM CINEMA</title>
	<link rel=stylesheet href="css/reset.css">
	<link rel="stylesheet" href="css/normalize.css">
    <link rel="stylesheet" href="css/skeleton.css">
	<link rel=stylesheet href="css/style.css">
	<link rel=stylesheet href="css/styleCalendario.css">
	<link rel=stylesheet href="css/styleCarrusel.css">

	<script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
	<script src="./js/funcionesCarrusel.js"></script>
	<script src="./js/funcionesCalendario.js"></script>
	<link rel="icon" type="image/png" href="img/icon.png">
</head>

<body>
	<header>
	
		<img src="img/Logos_Cinemes/IAM_CAT_logos_purple_rgb_1.png" class="logo" alt="IAM">
		<img src="img/titolCinema.png" class="titol" alt="I AM Cinemac">
		<a class="button botonHeader" title="admin / ausias" href="html/Admin/seleccioSessio.php">Administració</a>
		<a class="button botonHeader" href="html/Usuari/mail.php">Les teves entrades</a>
		<a class="button botonHeader" href="html/Cartelera/cartelera.php">Cartellera</a>

	</header>

	<div class="content">
		
		<h1>Pàgina principal</h1>

		<!--MOSTRAR CARRUSEL-->
		<div class='slideshow-container'>
		<a class='prev' onclick='plusSlides(-1)'>&lt;</a>
			<form action='html/Usuari/infoSessioSeleccionada.php' class="formulariPeliculaCartelera" method='post'>
				<?php
					echo $carrusel;
				?>
			</form>
			<a class='next' onclick='plusSlides(1)'>></a>
		</div>
		

		<br>
		<!--MOSTRAR CALENDARIO MENSUAL-->
    	<div class="row">
			<div class="two columns centro">
				<button id="retroceder" type="button" class="">Anterior</button>
			</div>
			<div class="eight columns">
				<form action='html/Usuari/mail.php' method='post'>
					<div id="calendario"></div>
				</form>
			</div>
			
			<div class="two columns centro">
				<button id="avanzar" type="button" class="">Següent</button>
			</div> 
		</div>

	</div>

	<?php include("html/Includes/footer.php"); ?>
</body>

</html>

<?php

?>